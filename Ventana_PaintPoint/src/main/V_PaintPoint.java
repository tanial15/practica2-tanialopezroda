package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.Window.Type;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import java.awt.Panel;
import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.TextField;
import java.awt.TextArea;
import javax.swing.JLayeredPane;
import javax.swing.JScrollBar;
import javax.swing.JRadioButtonMenuItem;
import java.awt.Scrollbar;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JList;
import java.awt.List;
import java.awt.Choice;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import java.awt.Checkbox;
import javax.swing.ButtonGroup;
import javax.swing.JSlider;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class V_PaintPoint extends JFrame {

	private JPanel contentPane;
	private JTextField txtFormas;
	private JTextField txtTipografia;
	private JTextField txtEmoticonos;
	private JTextField txtFuente;
	private JTextField txtHerramineta;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtTamao;
	private JTextField txtTamao_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_PaintPoint frame = new V_PaintPoint();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_PaintPoint() {
		setForeground(new Color(0, 0, 0));
		setFont(new Font("Berlin Sans FB", Font.PLAIN, 12));
		setTitle("PAINTPOINT");
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_PaintPoint.class.getResource("/Imagen/02-icone-pincel.png")));
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1209, 792);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 26));
		menuBar.setForeground(SystemColor.textHighlightText);
		menuBar.setBackground(SystemColor.activeCaptionText);
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		mnNewMenu.setForeground(SystemColor.text);
		mnNewMenu.setBackground(SystemColor.text);
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Nuevo");
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnNewMenu.add(mntmAbrir);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnNewMenu.add(mntmGuardar);
		
		JMenu mnGuardarComo = new JMenu("Guardar como...");
		mnNewMenu.add(mnGuardarComo);
		
		JMenuItem mntmImagenPng = new JMenuItem("Imagen PNG");
		mnGuardarComo.add(mntmImagenPng);
		
		JMenuItem mntmImagenJpg = new JMenuItem("Imagen JPG");
		mnGuardarComo.add(mntmImagenJpg);
		
		JMenuItem mntmImagenGif = new JMenuItem("Imagen GIF");
		mnGuardarComo.add(mntmImagenGif);
		
		JMenu mnEnv = new JMenu("Imprimir...");
		mnNewMenu.add(mnEnv);
		
		JMenuItem mntmImprimir = new JMenuItem("Imprimir");
		mnEnv.add(mntmImprimir);
		
		JMenuItem mntmVistaPreviaDe = new JMenuItem("Vista previa de impresion");
		mnEnv.add(mntmVistaPreviaDe);
		
		JMenuItem mntmEnviarPorCorreo = new JMenuItem("Enviar por correo electronico");
		mnNewMenu.add(mntmEnviarPorCorreo);
		
		JMenu mnNewMenu_1 = new JMenu("Edici\u00F3n");
		mnNewMenu_1.setForeground(SystemColor.text);
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mnNewMenu_1.add(mntmCortar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnNewMenu_1.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnNewMenu_1.add(mntmPegar);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mnNewMenu_1.add(mntmDeshacer);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mnNewMenu_1.add(mntmRehacer);
		
		JMenu mnNewMenu_2 = new JMenu("Ver");
		mnNewMenu_2.setForeground(SystemColor.text);
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmPantallaCompleta = new JMenuItem("Pantalla completa");
		mnNewMenu_2.add(mntmPantallaCompleta);
		
		JMenuItem mntmAlejar = new JMenuItem("Alejar");
		mnNewMenu_2.add(mntmAlejar);
		
		JMenuItem mntmAcrecar = new JMenuItem("Acercar");
		mnNewMenu_2.add(mntmAcrecar);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 102, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(15, 16, 360, 593);
		contentPane.add(tabbedPane);
		
		JTextArea textArea_1 = new JTextArea();
		tabbedPane.addTab("New tab", null, textArea_1, null);
		
		JTextArea textArea = new JTextArea();
		tabbedPane.addTab("New tab", null, textArea, null);
		
		txtFormas = new JTextField();
		txtFormas.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 20));
		txtFormas.setForeground(new Color(255, 255, 255));
		txtFormas.setBackground(new Color(0, 0, 0));
		txtFormas.setText("Formas:");
		txtFormas.setBounds(455, 74, 114, 26);
		contentPane.add(txtFormas);
		txtFormas.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(V_PaintPoint.class.getResource("/Imagen/Anotaci\u00F3n 2019-02-03 112247.png")));
		lblNewLabel.setBounds(408, 116, 230, 132);
		contentPane.add(lblNewLabel);
		
		txtTipografia = new JTextField();
		txtTipografia.setText("Tipografia:");
		txtTipografia.setForeground(Color.WHITE);
		txtTipografia.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 20));
		txtTipografia.setColumns(10);
		txtTipografia.setBackground(Color.BLACK);
		txtTipografia.setBounds(447, 277, 130, 26);
		contentPane.add(txtTipografia);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Calibri", "Times New Roman", "Arial", "Berlin Sans FB", "Cambria", "Garamond", "Georgia", "Tahoma", "Verdana"}));
		comboBox.setEditable(true);
		comboBox.setBounds(433, 319, 165, 51);
		contentPane.add(comboBox);
		
		txtEmoticonos = new JTextField();
		txtEmoticonos.setText("Emoticonos:");
		txtEmoticonos.setForeground(Color.WHITE);
		txtEmoticonos.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 20));
		txtEmoticonos.setColumns(10);
		txtEmoticonos.setBackground(Color.BLACK);
		txtEmoticonos.setBounds(455, 404, 130, 26);
		contentPane.add(txtEmoticonos);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(V_PaintPoint.class.getResource("/Imagen/Anotaci\u00F3n 2019-02-03 114315.png")));
		lblNewLabel_1.setBounds(408, 445, 265, 164);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon(V_PaintPoint.class.getResource("/Imagen/Anotaci\u00F3n 2019-02-03 114505.png")));
		lblNewLabel_2.setBounds(677, 72, 103, 475);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon(V_PaintPoint.class.getResource("/Imagen/Anotaci\u00F3n 2019-02-03 114518.png")));
		lblNewLabel_3.setBounds(687, 552, 96, 57);
		contentPane.add(lblNewLabel_3);
		
		txtFuente = new JTextField();
		txtFuente.setText("Fuente:");
		txtFuente.setForeground(Color.WHITE);
		txtFuente.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 20));
		txtFuente.setColumns(10);
		txtFuente.setBackground(Color.BLACK);
		txtFuente.setBounds(810, 44, 139, 26);
		contentPane.add(txtFuente);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(810, 96, 166, 143);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Subrayado");
		chckbxNewCheckBox.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		chckbxNewCheckBox.setBackground(new Color(255, 255, 255));
		chckbxNewCheckBox.setBounds(0, 0, 165, 29);
		panel.add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Negrita");
		chckbxNewCheckBox_1.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		chckbxNewCheckBox_1.setBackground(new Color(255, 255, 255));
		chckbxNewCheckBox_1.setBounds(0, 37, 165, 29);
		panel.add(chckbxNewCheckBox_1);
		
		JCheckBox chckbxNewCheckBox_2 = new JCheckBox("Sombra");
		chckbxNewCheckBox_2.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		chckbxNewCheckBox_2.setBackground(new Color(255, 255, 255));
		chckbxNewCheckBox_2.setBounds(0, 77, 165, 29);
		panel.add(chckbxNewCheckBox_2);
		
		JCheckBox chckbxNewCheckBox_3 = new JCheckBox("Cursiva");
		chckbxNewCheckBox_3.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		chckbxNewCheckBox_3.setBackground(new Color(255, 255, 255));
		chckbxNewCheckBox_3.setBounds(0, 114, 165, 29);
		panel.add(chckbxNewCheckBox_3);
		
		txtHerramineta = new JTextField();
		txtHerramineta.setText("Herramienta:");
		txtHerramineta.setForeground(Color.WHITE);
		txtHerramineta.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 20));
		txtHerramineta.setColumns(10);
		txtHerramineta.setBackground(Color.BLACK);
		txtHerramineta.setBounds(822, 281, 154, 26);
		contentPane.add(txtHerramineta);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setBounds(810, 323, 175, 151);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Cray\u00F3n");
		rdbtnNewRadioButton.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		rdbtnNewRadioButton.setBackground(new Color(255, 255, 255));
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(0, 0, 155, 29);
		panel_1.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Pincel");
		rdbtnNewRadioButton_1.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		rdbtnNewRadioButton_1.setBackground(new Color(255, 255, 255));
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(0, 37, 155, 29);
		panel_1.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("A\u00E9rografo");
		rdbtnNewRadioButton_2.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		rdbtnNewRadioButton_2.setBackground(new Color(255, 255, 255));
		buttonGroup.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.setBounds(0, 74, 155, 29);
		panel_1.add(rdbtnNewRadioButton_2);
		
		JRadioButton rdbtnNewRadioButton_3 = new JRadioButton("L\u00E1piz");
		rdbtnNewRadioButton_3.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 18));
		rdbtnNewRadioButton_3.setBackground(new Color(255, 255, 255));
		buttonGroup.add(rdbtnNewRadioButton_3);
		rdbtnNewRadioButton_3.setBounds(0, 111, 155, 29);
		panel_1.add(rdbtnNewRadioButton_3);
		
		txtTamao = new JTextField();
		txtTamao.setText("Tama\u00F1o:");
		txtTamao.setForeground(Color.WHITE);
		txtTamao.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 20));
		txtTamao.setColumns(10);
		txtTamao.setBackground(Color.BLACK);
		txtTamao.setBounds(1268, 471, 154, 26);
		contentPane.add(txtTamao);
		
		JSlider slider = new JSlider();
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setSnapToTicks(true);
		slider.setValue(1);
		slider.setBackground(new Color(0, 0, 0));
		slider.setMinimum(1);
		slider.setMaximum(10);
		slider.setBounds(795, 540, 200, 69);
		contentPane.add(slider);
		
		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 22));
		btnNewButton.setBackground(SystemColor.info);
		btnNewButton.setForeground(SystemColor.desktop);
		btnNewButton.setBounds(469, 636, 154, 45);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Imprimir");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton_1.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 22));
		btnNewButton_1.setBackground(SystemColor.info);
		btnNewButton_1.setForeground(new Color(0, 0, 0));
		btnNewButton_1.setBounds(727, 636, 147, 45);
		contentPane.add(btnNewButton_1);
		
		txtTamao_1 = new JTextField();
		txtTamao_1.setText("Tama\u00F1o:");
		txtTamao_1.setForeground(Color.WHITE);
		txtTamao_1.setFont(new Font("Berlin Sans FB Demi", Font.PLAIN, 20));
		txtTamao_1.setColumns(10);
		txtTamao_1.setBackground(Color.BLACK);
		txtTamao_1.setBounds(820, 498, 139, 26);
		contentPane.add(txtTamao_1);
	}
	public Choice getList() {
		return getList();
	}
}
